CREATE TABLE userList (
  id int NOT NULL AUTO_INCREMENT,
  name VARCHAR(32),
  email VARCHAR(255),
  password VARCHAR(255),
  PRIMARY KEY(id),
  CONSTRAINT email_unique UNIQUE (email)
);

CREATE TABLE movies (
   movieID INT NOT NULL AUTO_INCREMENT,
   title VARCHAR(255),
   releaseYear INT,
   director VARCHAR(255),
   genre VARCHAR(255),
   PRIMARY KEY(movieID)
);
INSERT INTO movies (title, director, genre, releaseYear) VALUES ("Rambo First Blood", "Ted Kotcheff", "Action", 1982);