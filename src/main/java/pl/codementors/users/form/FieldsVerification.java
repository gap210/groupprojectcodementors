package pl.codementors.users.form;

import javafx.scene.control.TextField;
import org.apache.commons.validator.routines.EmailValidator;


public class FieldsVerification {

    public static boolean isValidEmailAddress(String email) {

        boolean allowLocal = true;
        if (EmailValidator.getInstance(allowLocal).isValid(email)) {
            return true;
        } else return false;
    }


    public static boolean isNameNotEmpty(TextField name) {
        return !name.getText().isEmpty();
    }

    public static boolean isPasswordNotEmpty(TextField password) {
        return !password.getText().isEmpty();
    }
}
