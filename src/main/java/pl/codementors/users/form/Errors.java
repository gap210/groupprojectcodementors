package pl.codementors.users.form;

import java.util.ArrayList;
import java.util.List;

public class Errors {

    private List<String> errors;

    public Errors() {
        errors = new ArrayList<>();
    }

    public List<String> getErrors() {
        return errors;
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }

    public void addError(String error) {
        errors.add(error);
    }
}
