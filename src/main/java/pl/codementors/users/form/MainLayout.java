package pl.codementors.users.form;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import pl.codementors.users.jpa.MovieDao;
import pl.codementors.users.model.Movie;

import java.net.URL;
import java.util.ResourceBundle;

public class MainLayout implements Initializable {
    @FXML
    private TableView<Movie> movieTable;
    @FXML
    private TableColumn<Movie, String> genreColumn;
    @FXML
    private TableColumn<Movie, String> directorColumn;
    @FXML
    private TableColumn<Movie, Integer> releaseYearColumn;
    @FXML
    private TableColumn<Movie, String> titleColumn;

    private ObservableList<Movie> movies = FXCollections.observableArrayList();

    private ResourceBundle rb;

    private MovieDao dao = new MovieDao();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        movieTable.setItems(movies);
        movies.addAll(dao.findAll());
    }
}
