package pl.codementors.users.form;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import pl.codementors.users.jpa.UserDao;
import pl.codementors.users.model.User;
import pl.codementors.users.form.Errors;
import pl.codementors.users.service.ViewLoader;


public class CreateUserForm implements Initializable {

    @FXML
    private TextField name;
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField rePassword;
    private UserDao userDao;

    public void initialize(final URL location, final ResourceBundle resources) {
        userDao = new UserDao();
    }

    public void createUser(final ActionEvent event) throws IOException {
        Errors errors = new Errors();
        validate(errors);
        if (!errors.hasErrors()) {
            createUser();
            ViewLoader.loadLoginView((Node) event.getTarget());
        } else {
            showError("Alert", errors.getErrors().toString());
        }
    }

    private void createUser() {
        User user = new User(name.getText(), email.getText(), password.getText());
        Errors errors = new Errors();

        validate(errors);

        if (errors.hasErrors()) {
            errors.getErrors();
        } else {
            Optional<User> existingUser = userDao.findByEmail(email.getText());

            if (existingUser.isPresent()) {
                userAlreadyExists();
            } else {
                userDao.persist(user);
            }
        }

    }

    private void userAlreadyExists() {
        showError("User error", "User with given email already exists!");
    }

    private void showError(final String title, final String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, content);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.show();
    }

    private boolean validatePassword(Errors errors) {
        return password.getText().equals(rePassword.getText());
    }


    private void validate(Errors errors) {
        validateEmail(errors);
        validatePassword(errors);
        isNameNotEmpty(errors);
        isPasswordNotEmpty(errors);
    }

    private boolean validateEmail(Errors errors) {

        FieldsVerification ver = new FieldsVerification();
        if (ver.isValidEmailAddress(email.getText())) {
            return true;
        } else {
            errors.addError("Email nie teges");
            return false;
        }
    }

    public boolean isNameNotEmpty(Errors errors) {
        FieldsVerification ver = new FieldsVerification();
        if (ver.isNameNotEmpty(name)) {
            return true;
        } else {
            errors.addError("Imie nie teges");
            return false;
        }
    }

    public boolean isPasswordNotEmpty(Errors errors) {
        FieldsVerification ver = new FieldsVerification();
        if (ver.isPasswordNotEmpty(password)) {
            return true;
        } else {
            errors.addError("Hasło nie teges");
            return false;
        }
    }


}
