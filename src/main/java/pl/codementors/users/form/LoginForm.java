package pl.codementors.users.form;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import pl.codementors.users.jpa.UserDao;
import pl.codementors.users.model.User;
import pl.codementors.users.service.ViewLoader;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class LoginForm implements Initializable {

    @FXML
    private TextField email;
    @FXML
    private PasswordField password;

    private UserDao userDao;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        this.userDao = new UserDao();
    }

    public void onLogin(final ActionEvent event) throws IOException {
        Optional<User> user = userDao.findByEmailAndPassword(email.getText(), password.getText());

        if (user.isPresent()) {
            System.out.println("LOGGED IN");
            ViewLoader.loadMainLayoutView((Node) event.getTarget());
        } else {
            System.out.println("LOGIN FAILED");
        }
    }

    public void onCreateAccount(final ActionEvent event) throws IOException {
        ViewLoader.loadCreateUserView((Node) event.getTarget());
    }

}
