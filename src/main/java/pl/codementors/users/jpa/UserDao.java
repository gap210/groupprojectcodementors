package pl.codementors.users.jpa;

import java.util.Optional;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import pl.codementors.users.model.User;
import pl.codementors.users.model.User_;

public class UserDao {

    private static final Logger LOG = Logger.getLogger(UserDao.class.getName());

    private EntityManager getEntityManager() {
        return UsersEntityManagerFactory.createEntityManager();
    }

    public User persist(User user) {
        EntityManager entityManager = getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(user);
        transaction.commit();
        entityManager.close();

        return user;
    }

    public Optional<User> findByEmail(String email) {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery("SELECT u FROM User u WHERE u.email= :email");
        query.setParameter("email", email);

        Optional<User> singleResult;
        try {
            singleResult = Optional.ofNullable((User) query.getSingleResult());
        } catch (NoResultException e) {
            LOG.warning(e.getMessage());
            singleResult = Optional.empty();
        }
        entityManager.close();

        return singleResult;
    }

    public Optional<User> findByEmailAndPassword(final String email, final String password) {
        EntityManager entityManager = getEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> root = query.from(User.class);

        Predicate emailPredicate = criteriaBuilder.and(criteriaBuilder.equal(root.get(User_.email), email));
        Predicate passwordPredicate = criteriaBuilder.and(criteriaBuilder.equal(root.get(User_.password), password));
        query.where(emailPredicate, passwordPredicate);

        Optional<User> singleResult;
        try {
            singleResult = Optional.ofNullable(entityManager.createQuery(query).getSingleResult());
        } catch (NoResultException e) {
            LOG.warning(e.getMessage());
            singleResult = Optional.empty();
        }

        entityManager.close();

        return singleResult;

    }
}
