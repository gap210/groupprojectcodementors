package pl.codementors.users.jpa;

import pl.codementors.users.model.Movie;
import pl.codementors.users.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class MovieDao {
    private EntityManager getEntityManager() {
        return UsersEntityManagerFactory.createEntityManager();
    }

    public Movie persist(Movie movie) {
        EntityManager entityManager = getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(movie);
        transaction.commit();
        entityManager.close();
        return movie;
    }
    public void delete(Movie movie){
        EntityManager em = getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(movie));
        tx.commit();
        em.close();
    }
    public Movie merge(Movie movie){
        EntityManager em = getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.merge(movie);
        tx.commit();
        em.close();
        return movie;
    }
    public List<Movie> findAll(){
        EntityManager em = getEntityManager();
        Query query = em.createQuery("SELECT m FROM Movie m");
        List<Movie> moviesList = query.getResultList();
        em.close();
        return moviesList;
    }
}
