package pl.codementors.users.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class UsersEntityManagerFactory {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("UsersPU");

    public static EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    public static void close() {
        emf.close();
    }

}
