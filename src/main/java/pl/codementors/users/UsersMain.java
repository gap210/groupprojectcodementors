package pl.codementors.users;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class UsersMain extends Application {

    public static void main(String... args) {
        launch(args);
    }

  public void start(final Stage primaryStage) throws Exception {
    URL resource = UsersMain.class.getResource("/pl/codementors/users/login-form.fxml");
    ResourceBundle rb = ResourceBundle.getBundle("messages/msg");
    FXMLLoader loader = new FXMLLoader(resource, rb);

        Parent loginView = loader.load();

        Scene scene = new Scene(loginView);
        scene.getStylesheets().add("/view/styles.css");
        primaryStage.minWidthProperty().bind(scene.heightProperty());
        primaryStage.minHeightProperty().bind(scene.widthProperty());
        primaryStage.setScene(scene);

        primaryStage.show();
    }
}
