package pl.codementors.users.service;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import pl.codementors.users.UsersMain;

import java.io.IOException;
import java.util.ResourceBundle;

public class ViewLoader {

    public static void loadLoginView(Node node) throws IOException {
        loadView("/pl/codementors/users/login-form.fxml", node);
    }

    public static void loadCreateUserView(Node node) throws IOException {
        loadView("/pl/codementors/users/create-user-form.fxml", node);
    }

    public static void loadMainLayoutView(Node node) throws IOException {
        loadView("/pl/codementors/users/main-layout.fxml", node);
    }

    private static void loadView(String viewPath, Node node) throws IOException {
        ResourceBundle rb = ResourceBundle.getBundle("messages/msg");

        FXMLLoader loader = new FXMLLoader(UsersMain.class.getResource(viewPath), rb);
        Parent createUserView = loader.load();
        Scene scene = node.getScene();
        scene.setRoot(createUserView);
        Stage stage = (Stage) scene.getWindow();
        stage.sizeToScene();
    }
}
